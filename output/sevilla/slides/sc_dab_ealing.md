% "Waiting on the train" 
% *The anticipatory effects of Crossrail in Ealing*
% Sam Comber & Dani Arribas-Bel

#
##

[Dani Arribas-Bel](http://darribas.org) 
[**`@darribas`**]
 
Geographic Data Science Lab

Department of Geography and Planning

University of Liverpool 

#
## Today

* Aim
* The Geo Data Revolution as an "urban observatory"
* Context
* Empirical strategy
* Results
* Robustness checks
* Conclusion

#
## Aim

<span class='fragment'>
Evaluate **individuals' anticipation** when transport infrastructure interventions
are announced

<span class='fragment'>
Estimate the (**causal**) effects of announced upgrades in train stations on
house prices

<span class='fragment'>
Sign and size of the effect of **Crossrail** in the London borough of
**Ealing**

#
## The Geo Data Revolution

## Accidental Data

* Sensors carried by **individuals**
* Businesses moving (at least partly) **online**
* **Public entities** releasing more and more datasets for efficiency and transparency
  reasons <span class="fragment"> (**Open Data**)

 <span class="fragment">
$\rightarrow$ **Not** originally intended for research

 <span class="fragment">
<b>*"Always-on"* observatory</b> $\rightarrow$ the city as a lab to try out
things

## An "urban observatory"

<div style="height: 600px;" markdown="1">
![](fig/paid_price.png)
</div>

<!--

* Updated monthly!
* Address, location, and (some) characteristics

-->

#
## Context

## Crossrail

<center>
*"A new railway for London and the South East"*
</center>

<div class='fragment'>
* **Announced** in **2008** (Crossrail Act)
* **Scheduled** for full completion by **2019**
* Part of a **wider regional strategy** make more accessible London's main employment 
  areas <span class='fragment'> $\rightarrow$ **Ealing** gets it as a **"side effect"**
</div>

## Ealing

<div style="height: 800px;" markdown="1">
![](fig/XRRR.jpg)
</div>

## Data

* Land registry augmented with ONS and EDINA
* 50,864 transactions 2002-14: 31,435 pre and 19,429 post-announcement
* Paid price and the usual **structural**, **locational**
  and **environmental** characteristics of properties

## 

<div style="" markdown="1">
![](fig/XRQGIS.png)
</div>

##

<div style="" markdown="1">
![](fig/table.png)
</div>


#
## Empirical strategy

## 

<!--
Knn = 15
-->

<div class='fragment current-visible'>
<small>
<center>
$$
\log P_{it} = \alpha + dist_{CR} \; \boldsymbol{\beta_1} + X_{it} \; \beta_k + \varepsilon_{it}
$$
</center>
</small>
</div>

<div class='fragment current-visible'>
<small>
<center>
$$
\log P_{it} = \alpha + dist_{CR} \; \boldsymbol{\beta_1} + X_{it} \; \beta_k + u{it} \; ; \;
u_{it} = \lambda W u_{it} +  \varepsilon_{it}
$$
</center>
</small>
</div>

<div class='fragment current-visible'>
<small>
<center>
$$
\log P_{it} = \alpha + dist_{CR} \; \beta_1 + \beta_2 Post + \boldsymbol{\beta_3} \; dist_{CR} \times Post + X_{it} \; \beta_k + \varepsilon_{it}
$$
</center>
</small>
</div>

<div class='fragment current-visible'>
<small>
<center>
$$
\log P_{it} = \alpha + dist_{CR} \; \beta_1 + \beta_2 Post + \boldsymbol{\beta_3} \; dist_{CR} \times Post + X_{it} \; \beta_k + u_{it} \; ; \;
u_{it} = \lambda W u_{it} +  \varepsilon_{it}
$$
</center>
</small>
</div>
## {data-transition=none}

<small>
<center>
$$
\log P_{it} = \alpha + dist_{CR} \; \boldsymbol{\beta_1} + X_{it} \; \beta_k + \varepsilon_{it}
$$
</center>
</small>

<small>
<center>
$$
\log P_{it} = \alpha + dist_{CR} \; \boldsymbol{\beta_1} + X_{it} \; \beta_k + u{it} \; ; \;
u_{it} = \lambda W u_{it} +  \varepsilon_{it}
$$
</center>
</small>

<small>
<center>
$$
\log P_{it} = \alpha + dist_{CR} \; \beta_1 + \beta_2 Post + \boldsymbol{\beta_3} \; dist_{CR} \times Post + X_{it} \; \beta_k + \varepsilon_{it}
$$
</center>
</small>

<small>
<center>
$$
\log P_{it} = \alpha + dist_{CR} \; \beta_1 + \beta_2 Post + \boldsymbol{\beta_3} \; dist_{CR} \times Post + X_{it} \; \beta_k + u_{it} \; ; \;
u_{it} = \lambda W u_{it} +  \varepsilon_{it}
$$
</center>
</small>


#
## Results

## Baseline {data-transition=none}

<div style="" markdown="1">
![](fig/baseline.png)
</div>

## Baseline {data-transition=none}

<div style="" markdown="1">
![](fig/baseline_green.png)
</div>

## Spatial Baseline {data-transition=none}

<div style="" markdown="1">
![](fig/baseline_spatial_green.png)
</div>

## Diff-in-diff {data-transition=none}

<div style="" markdown="1">
![](fig/did.png)
</div>

## Diff-in-diff {data-transition=none}

<div style="" markdown="1">
![](fig/did_green.png)
</div>

## Spatial diff-in-diff {data-transition=none}

<div style="" markdown="1">
![](fig/spatial_did_green.png)
</div>

##

<div style="" markdown="1">
![](fig/quarterplot.svg)
</div>

#
## Conclusion

* **Crossrail** is a (very) large transport investment
* **Anticipation** (causal) effect on house prices
* For every Km a house is closer to a station 
  targetted for Crossrail upgrades, the home-buyers are 
  willing to pay between **2.56%** and **2.76%** extra (down from the naive
  $\approx 4\%$ but still significant).

#
##

[Dani Arribas-Bel](http://darribas.org) 
[(`@darribas`)](http://twitter.com/darribas)
 
Geographic Data Science Lab

Department of Geography and Planning

University of Liverpool 


