# *'Waiting on the train'*

This repository contains reproducible code and data for the paper "'Waiting on the train': The anticipatory (causal) effects of Crossrail in Ealing", by Sam Comber and [Dani Arribas-Bel](http://darribas.org).

The paper is published with an open access license by the Journal of Transport Geography:

> [http://www.sciencedirect.com/science/article/pii/S0966692317300820](http://www.sciencedirect.com/science/article/pii/S0966692317300820)

## Citation

Please use the following citation for the paper and code:

```
@article{COMBER201713,
	title = "�Waiting on the train�: The anticipatory (causal) effects of Crossrail in Ealing",
	journal = "Journal of Transport Geography",
	volume = "64",
	number = "",
	pages = "13 - 22",
	year = "2017",
	note = "",
	issn = "0966-6923",
	doi = "http://dx.doi.org/10.1016/j.jtrangeo.2017.08.004",
	url = "http://www.sciencedirect.com/science/article/pii/S0966692317300820",
	author = "Sam Comber and Dani Arribas-Bel",
	keywords = "Transport intervention",
	keywords = "Willingness-to-pay",
	keywords = "Spatial econometrics",
	keywords = "Difference-in-difference model"
}
```

## License

Same as the paper, the code and documentation in this repository are released under a [CC-BY](https://creativecommons.org/licenses/by/4.0/) open license.

